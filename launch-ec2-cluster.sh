#!/bin/bash

/root/spark/bin/spark-submit \
    --master spark://ec2-$1.compute-1.amazonaws.com:7077 \
    --driver-memory 1g \
    --class "pl.edu.agh.student.mizmuda.tpr.classify.SparseNaiveBayes" \
    /root/tpr-spark-assembly-1.0.jar \
    --numPartitions 2 --numFeatures 10 --lambda 0.1 --dataDoublingCount 0 /breast-cancer-$2 \
| grep RESULT >> results.txt
