package pl.edu.agh.student.mizmuda.tpr.scrape

import java.io.File

import com.google.gson.Gson
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.twitter.TwitterUtils
import org.apache.spark.streaming.{Seconds, StreamingContext, Time}
import org.apache.spark.{SparkConf, SparkContext}
import pl.edu.agh.student.mizmuda.tpr.AbstractParams
import scopt.OptionParser
import twitter4j.auth.OAuthAuthorization
import twitter4j.conf.ConfigurationBuilder

/**
 * Collect at least the specified number of tweets into json text files.
 */
object TweetCollector {
  val defaultParams = Params()
  val parser = new OptionParser[Params]("TweetCollector") {
    head("TwitterScraper: simple tool for persisting tweets")
    opt[Int]("numTweetsToCollect")
      .text("")
      .action((x, c) => c.copy(numTweetsToCollect = x))
    opt[Int]("intervalSecs")
      .text("")
      .action((x, c) => c.copy(intervalSecs = x))
    opt[Int]("partitionsEachInterval")
      .text("")
      .action((x, c) => c.copy(partitionsEachInterval = x))
    opt[String]("consumerKey")
      .text("")
      .required()
      .action((x, c) => c.copy(consumerKey = x))
    opt[String]("consumerSecret")
      .text("")
      .required()
      .action((x, c) => c.copy(consumerSecret = x))
    opt[String]("accessToken")
      .text("")
      .required()
      .action((x, c) => c.copy(accessToken = x))
    opt[String]("accessTokenSecret")
      .text("")
      .required()
      .action((x, c) => c.copy(accessTokenSecret = x))
    arg[String]("<outputDirectory>")
      .text("")
      .required()
      .action((x, c) => c.copy(outputDirectory = x))
  }

  private var numTweetsCollected = 0L
  private var partNum = 0
  private var gson = new Gson()

  def main(args: Array[String]) {
    parser.parse(args, defaultParams).map { params =>
      run(params)
    }.getOrElse {
      sys.exit(1)
    }
  }

  def run(params: Params) {

    prepareOutputDir(params.outputDirectory)

    val conf = new SparkConf().setAppName(this.getClass.getSimpleName)
    val context = new SparkContext(conf)
    val streamingContext = new StreamingContext(context, Seconds(params.intervalSecs))

    val authorization: Some[OAuthAuthorization] = getAuth(params)
    val tweetStream = TwitterUtils.createStream(streamingContext, authorization).map(gson.toJson(_))
    tweetStream.foreachRDD(tweetRddHandler(params.outputDirectory, params.numTweetsToCollect, params.partitionsEachInterval))

    streamingContext.start()
    streamingContext.awaitTermination()
  }

  def getAuth(params: Params): Some[OAuthAuthorization] = {
    System.setProperty("twitter4j.oauth.consumerKey", params.consumerKey)
    System.setProperty("twitter4j.oauth.consumerSecret", params.consumerSecret)
    System.setProperty("twitter4j.oauth.accessToken", params.accessToken)
    System.setProperty("twitter4j.oauth.accessTokenSecret", params.accessTokenSecret)

    Some(new OAuthAuthorization(new ConfigurationBuilder().build()))
  }

  def tweetRddHandler(outputDirectory: Any, numTweetsToCollect: Int, partitionsEachInterval: Int): (RDD[String], Time) => Unit = {
    (rdd, time) => {
      val count = rdd.count()
      if (count > 0) {
        val outputRDD = rdd.repartition(partitionsEachInterval)
        outputRDD.saveAsTextFile(outputDirectory + "/tweets_" + time.milliseconds.toString)
//        outputRDD.collect().foreach(println)
        numTweetsCollected += count
        if (numTweetsCollected > numTweetsToCollect) {
          System.exit(0)
        }
      }
    }
  }

  def prepareOutputDir(outputDirectory: Any): Unit = {
    val outputDir = new File(outputDirectory.toString)
    if (outputDir.exists()) {
      System.err.println("ERROR - %s already exists: delete or specify another directory".format(
        outputDirectory))
      System.exit(1)
    }
    outputDir.mkdirs()
  }

  def verifyCommandLine(args: Array[String]): Unit = {
    if (args.length < 3) {
      System.err.println("Usage: " + this.getClass.getSimpleName +
        "<outputDirectory> <numTweetsToCollect> <intervalInSeconds> <partitionsEachInterval>")
      System.exit(1)
    }
  }

  case class Params(outputDirectory: String = null,
                    consumerKey: String = null,
                    consumerSecret: String = null,
                    accessToken: String = null,
                    accessTokenSecret: String = null,
                    numTweetsToCollect: Int = 10,
                    intervalSecs: Int = 10,
                    partitionsEachInterval: Int = 1) extends AbstractParams[Params]

}