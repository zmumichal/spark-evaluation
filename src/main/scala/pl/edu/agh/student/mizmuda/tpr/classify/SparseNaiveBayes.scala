package pl.edu.agh.student.mizmuda.tpr.classify

import org.apache.log4j.{Level, Logger}
import org.apache.spark.mllib.classification.NaiveBayes
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import pl.edu.agh.student.mizmuda.tpr.AbstractParams
import scopt.OptionParser

object SparseNaiveBayes {

  def main(args: Array[String]) {
    val defaultParams = Params()

    val parser = new OptionParser[Params]("SparseNaiveBayes") {
      head("SparseNaiveBayes: an example naive Bayes app for LIBSVM data.")
      opt[Int]("dataDoublingCount")
        .text("number of training dataset being doubled (n mean dataset will be repeated 2^n times)")
        .action((x, c) => c.copy(dataDoublingCount = x))
      opt[Int]("numPartitions")
        .text("min number of partitions")
        .action((x, c) => c.copy(minPartitions = x))
      opt[Int]("numFeatures")
        .text("number of features")
        .action((x, c) => c.copy(numFeatures = x))
      opt[Double]("lambda")
        .text(s"lambda (smoothing constant), default: ${defaultParams.lambda}")
        .action((x, c) => c.copy(lambda = x))
      arg[String]("<input>")
        .text("input paths to labeled examples in LIBSVM format")
        .required()
        .action((x, c) => c.copy(input = x))
    }

    parser.parse(args, defaultParams).map { params =>
      run(params)
    }.getOrElse {
      sys.exit(1)
    }
  }

  def run(params: Params) {
    val start = System.currentTimeMillis()

    val conf = new SparkConf().setAppName(s"SparseNaiveBayes with $params")
    val sc = new SparkContext(conf)

    Logger.getRootLogger.setLevel(Level.WARN)

    val minPartitions =
      if (params.minPartitions > 0) params.minPartitions else sc.defaultMinPartitions

    val examples =
      MLUtils.loadLibSVMFile(sc, params.input, params.numFeatures, minPartitions)
    // Cache examples because it will be used in both training and evaluation.
    examples.cache()

    val splits = examples.randomSplit(Array(0.8, 0.2))
    val training: RDD[LabeledPoint] = splits(0)

    var repeatedTriaining = training
    for (a <- 1 to params.dataDoublingCount) {
      repeatedTriaining = repeatedTriaining ++ repeatedTriaining
    }
    val test = splits(1)

    val numTraining = repeatedTriaining.count()
    val numTest = test.count()
    val numTotal = examples.count()

    println(s"numTraining = $numTraining, numTest = $numTest.")
    val afterLoadingAndDuplication = System.currentTimeMillis()
    val model = new NaiveBayes().setLambda(params.lambda).run(repeatedTriaining)

    val prediction = model.predict(test.map(_.features))
    val predictionAndLabel = prediction.zip(test.map(_.label))
    val accuracy = predictionAndLabel.filter(x => x._1 == x._2).count().toDouble / numTest

    println(s"Test accuracy = $accuracy.")
    val duration: Long = System.currentTimeMillis() - start
    val durationWithoutLoading: Long = System.currentTimeMillis() - afterLoadingAndDuplication

    println(s"\nRESULT: dataset_size: $numTotal, total_time: $duration, learning_time: $durationWithoutLoading\n")

    sc.stop()
  }

  case class Params(input: String = null,
                    dataDoublingCount: Int = 0,
                    minPartitions: Int = 0,
                    numFeatures: Int = -1,
                    lambda: Double = 1.0) extends AbstractParams[Params]

}