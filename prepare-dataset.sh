#!/bin/bash

N=$1

#wget http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary/breast-cancer
hadoop=/root/ephemeral-hdfs/bin/hadoop
${hadoop} fs -cp file:///root/breast-cancer /breast-cancer
seq ${N} | xargs -i -- cat breast-cancer | ${hadoop} fs -put - /breast-cancer-${N}
